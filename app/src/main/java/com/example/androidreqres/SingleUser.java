package com.example.androidreqres;

import com.google.gson.annotations.SerializedName;

public class SingleUser {
    public User getUser() {
        return user;
    }

    @SerializedName("data")
    private User user;
}
