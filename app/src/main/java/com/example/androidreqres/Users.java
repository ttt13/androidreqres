package com.example.androidreqres;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Users {

    @SerializedName("data")
    private List<User> data;

    public List<User> getData() {
        return data;
    }
}
