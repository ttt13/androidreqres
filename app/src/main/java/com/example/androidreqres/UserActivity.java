package com.example.androidreqres;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserActivity extends AppCompatActivity {
    private int id;
    private UserService userService = UserService.getInstance();
    private TextView firstName;
    private TextView lastName;
    private TextView email;
    private TextView nameHeader;
    private ImageView avatar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        Intent intent = getIntent();
        id = intent.getIntExtra("id", -1);

        setupIds();
        getUserInfo();
    }

    private void setupIds() {
        nameHeader = findViewById(R.id.userNameTextView);
        firstName = findViewById(R.id.userFirstNameFillTextView);
        lastName = findViewById(R.id.lastNameFillTextView);
        email = findViewById(R.id.emailFillTextView);
        avatar = findViewById(R.id.userDisplayPictureImageView);
    }

    private void getUserInfo() {
        userService.getUserAPI().getUser(id).enqueue(new Callback<SingleUser>() {
            @Override
            public void onResponse(Call<SingleUser> call, Response<SingleUser> response) {
                if (response.isSuccessful()) {
                    SingleUser singleUser = response.body();
                    User user = singleUser.getUser();

                    nameHeader.setText(user.getFirstName() + " " + user.getLastName());
                    firstName.setText(user.getFirstName());
                    lastName.setText(user.getLastName());
                    email.setText(user.getEmail());
                    Picasso.get().load(user.getAvatar()).into(avatar);
                }
            }

            @Override
            public void onFailure(Call<SingleUser> call, Throwable t) {
            }
        });
    }
}