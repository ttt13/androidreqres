package com.example.androidreqres;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UserService {

    private static UserService userService = null;
    private UserAPI userAPI;

    private UserService() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(userAPI.url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        userAPI = retrofit.create(UserAPI.class);
    }

    public static synchronized UserService getInstance() {
        if (userService == null) {
            userService = new UserService();
        }
        return userService;
    }

    public UserAPI getUserAPI() {
        return userAPI;
    }
}
