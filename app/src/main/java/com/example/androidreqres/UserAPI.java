package com.example.androidreqres;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface UserAPI {
    String url = "https://reqres.in/";

    @GET("api/users/")
    Call<Users> getUsers();

    @GET("api/users/{id}")
    Call<SingleUser> getUser(@Path("id") int id);
}
