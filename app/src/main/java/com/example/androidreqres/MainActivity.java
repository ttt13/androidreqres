package com.example.androidreqres;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements UserListItemAdapter.OnItemListener {

    private RecyclerView userRecyclerView;
    private UserService userService = UserService.getInstance();
    private RecyclerView.LayoutManager layoutManager;
    private UserListItemAdapter userListItemAdapter;
    Users users = new Users();
    List<User> userList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        setupRecyclerView();
        getUsers();
    }

    private void setupRecyclerView() {
        userRecyclerView = findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(getApplicationContext());
        userListItemAdapter = new UserListItemAdapter(userList, this);
        userRecyclerView.setLayoutManager(layoutManager);
        userRecyclerView.setAdapter(userListItemAdapter);
    }

    private void getUsers() {
        userService.getUserAPI().getUsers().enqueue(new Callback<Users>() {
            @Override
            public void onResponse(Call<Users> call, Response<Users> response) {
                if (response.isSuccessful()) {
                    Users usersList = response.body();
                    userList.addAll(usersList.getData());
                }
                userListItemAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<Users> call, Throwable t) {
            }
        });
    }

    @Override
    public void onItemClick(int position) {
        Intent intent = new Intent(getApplicationContext(), UserActivity.class);
        User user = userListItemAdapter.getUser(position);
        intent.putExtra("id", user.getId());
        startActivity(intent);
    }
}