package com.example.androidreqres;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class UserListItemAdapter extends RecyclerView.Adapter<UserListItemAdapter.UserItemViewHolder> {
    private List<User> users;
    private OnItemListener onItemListener;

    public UserListItemAdapter(List<User> userList, OnItemListener onItemListener) {
        this.users = userList;
        this.onItemListener = onItemListener;
    }

    public User getUser(int position) {
        return users.get(position);
    }

    @NonNull
    @Override
    public UserItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_item, parent, false);
        UserItemViewHolder evh = new UserItemViewHolder(v, onItemListener);
        return evh;
    }

    @Override
    public void onBindViewHolder(@NonNull UserItemViewHolder holder, int position) {
        User current = users.get(position);
        holder.nameTextView.setText(current.getFirstName() + " " + current.getLastName());
        Picasso.get().load(current.getAvatar()).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public interface OnItemListener {
        void onItemClick(int position);
    }

    public static class UserItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView nameTextView;
        public ImageView imageView;
        OnItemListener onItemListener;

        public UserItemViewHolder(@NonNull View itemView, OnItemListener onItemListener) {
            super(itemView);
            nameTextView = itemView.findViewById(R.id.textViewFullName);
            imageView = itemView.findViewById(R.id.imageViewPhoto);
            this.onItemListener = onItemListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            onItemListener.onItemClick(getAdapterPosition());
        }
    }
}


