# Android ReqRes by Peter
## Technologies used:

- Java, Android Studio
- Retrofit
- Picasso for image downloading: https://square.github.io/picasso/

# Android ReqRes!

Hi! Let's create an Android application that shows data from **ReqRes.in**. Fork this repo to your local directory. Once you finish, please submit a **Pull Request** to this repo.
## API

 - https://reqres.in/api/users to get all users
 - https://reqres.in/api/users/2 to get a user with the id 2

## Android

Create an app named AndroidReqRes, that has 2 activities

 - **MainActivity** which shows the list of users in a card style. A card only contains the avatar image, first name and last name. Clicking on a card should open **UserActivity**
 - **UserActivity** which shows the avatar, first name, last name, and email respectively

Kotlin, Retrofit and Jetpack Compose are recommended. Design the look as you feel fit.